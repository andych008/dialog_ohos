package wang.unclecat.dialog_ohos;

import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/**
 * Toast工具类
 *
 * @author: 喵叔catuncle
 */
public class ToastUtil {

    public static void show(Context context, String text) {
        new ToastDialog(context)
                .setText(text)
                .setAlignment(LayoutAlignment.CENTER)
                .show();
    }
}
