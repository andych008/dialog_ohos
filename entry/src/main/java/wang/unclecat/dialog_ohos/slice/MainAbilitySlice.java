package wang.unclecat.dialog_ohos.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.window.dialog.ListDialog;
import timber.log.Timber;
import wang.unclecat.catdialog.CatListDialog;
import wang.unclecat.catdialog.SimpleItemProvider;
import wang.unclecat.dialog_ohos.MyItemProvider;
import wang.unclecat.dialog_ohos.ResourceTable;
import wang.unclecat.dialog_ohos.ToastUtil;

import java.util.Arrays;

public class MainAbilitySlice extends AbilitySlice {

    private SimpleItemProvider simpleItemProvider = new SimpleItemProvider(Arrays.asList(new String[]{"选项1", "选项2"}));
    private MyItemProvider myItemProvider = new MyItemProvider(Arrays.asList(new String[]{"选项1", "选项2"}));

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        //默认的RadioButton显示异常
        findComponentById(ResourceTable.Id_btnListDialog0)
                .setClickedListener(component -> {
                    ListDialog listDialog = new ListDialog(MainAbilitySlice.this, ListDialog.SINGLE);
                    listDialog.setSingleSelectItems(new String[]{"选项1", "选项2"}, 0);
                    listDialog.setOnSingleSelectListener((iDialog, i) -> Timber.d("OnItemClicked: i = [ %s ]", i));
                    listDialog.setButton(0, "取消", (iDialog, i) -> iDialog.hide());
                    listDialog.setButton(1, "确认", (iDialog, i) -> {
                        ListContainer listContainer = (ListContainer) listDialog.getListContainer();
                        int selectedItemIndex = listContainer.getSelectedItemIndex();
                        ToastUtil.show(MainAbilitySlice.this, "选择了第" + (selectedItemIndex + 1) + "个");
                        iDialog.hide();
                    });
                    listDialog.show();
                });

        findComponentById(ResourceTable.Id_btnListDialog1)
                .setClickedListener(component -> {
                    new CatListDialog(MainAbilitySlice.this)
                            .setItemProvider(simpleItemProvider, -1)
                            .setItemClickedListener((iDialog, i) -> Timber.d("OnItemClicked: i = [ %s ]", i))
                            .setButton(0, "取消")
                            .setButton(1, "确认",
                                    (iDialog, selectedIndex) -> ToastUtil.show(MainAbilitySlice.this, "选择了第" + (selectedIndex + 1) + "个"))
                            .show();
                });

        findComponentById(ResourceTable.Id_btnListDialog2)
                .setClickedListener(component -> {
                    new CatListDialog(MainAbilitySlice.this)
                            .setItemProvider(myItemProvider, 0)//默认选中第1个
                            .setItemClickedListener((iDialog, i) -> Timber.d("OnItemClicked: i = [ %s ]", i))
                            .handleSelect(false)
                            .setSelectStateChangedListener((dialog, preIndex, index) ->
                                    myItemProvider.handleSelect(dialog.getListContainer(), preIndex, index))
                            .setButton(0, "取消")
                            .setButton(1, "确认",
                                    (iDialog, selectedIndex) -> ToastUtil.show(MainAbilitySlice.this, "选择了第" + (selectedIndex + 1) + "个"))
                            .show();
                });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
