# 简介

鸿蒙ListDialog（单选列表）显示有问题，选中需要点击两次。所以，我封装了CatDialog。

上图吧：

<img src="https://gitee.com/andych008/pic/raw/master/img/dialog_ohos_01.jpg" style="zoom:80%;" /> <img src="https://gitee.com/andych008/pic/raw/master/img/dialog_ohos_02.jpg" style="zoom:80%;" />

# 使用

1. 支持链式调用：

    ```
    new CatListDialog(MainAbilitySlice.this)
            .setItemProvider(simpleItemProvider, -1)//默认不选中
            .setButton(0, "取消")
            .setButton(1, "确认",
                    (iDialog, selectedIndex) -> ToastUtil.show(MainAbilitySlice.this, "选择了第" + (selectedIndex + 1) + "个"))
            .show();
    ```

2. 支持自定义数据、布局

    ```
    //通过自定义ItemProvider，来适配不同的数据、布局。
    new CatListDialog(MainAbilitySlice.this)
            .setItemProvider(myItemProvider, 0)//默认选中第1个
            .handleSelect(false)
            .setSelectStateChangedListener((dialog, preIndex, index) ->
                    myItemProvider.handleSelect(dialog.getListContainer(), preIndex, index))
            .setButton(0, "取消")
            .setButton(1, "确认",
                    (iDialog, selectedIndex) -> ToastUtil.show(MainAbilitySlice.this, "选择了第" + (selectedIndex + 1) + "个"))
            .show();
    ```
