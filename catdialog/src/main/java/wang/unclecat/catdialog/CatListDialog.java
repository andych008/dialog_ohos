package wang.unclecat.catdialog;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ListContainer;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.app.Context;

/**
 * 单选ListDialog
 *
 * @author: 喵叔catuncle
 */
public class CatListDialog extends CommonDialog {
    private int selectedItemIndex = 0;
    private ListContainer listContainer;
    private ItemClickedListener itemClickedListener;
    private OnSelectStateChangedListener selectStateChangedListener;
    private SimpleItemSelectHandler itemSelectHandler = new SimpleItemSelectHandler();
    private boolean isHandleSelect = true;

    public interface OnSelectStateChangedListener {
        void onSelectStateChanged(CatListDialog dialog, int preIndex, int index);
    }

    public CatListDialog(Context context) {
        super(context);
        setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        listContainer = new ListContainer(context);
        ListContainer.LayoutConfig layoutConfig = new ListContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        listContainer.setLayoutConfig(layoutConfig);
        super.setContentCustomComponent(listContainer);
    }

    public ListContainer getListContainer() {
        return listContainer;
    }

    public CatListDialog setItemProvider(BaseItemProvider itemProvider, int initSelectedIndex) {
        selectedItemIndex = initSelectedIndex;

        listContainer.setItemProvider(itemProvider);

        listContainer.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int index, long id) {
                if (itemClickedListener != null) {
                    itemClickedListener.onClick(CatListDialog.this, index);
                }
            }
        });

        listContainer.setItemSelectedListener(new ListContainer.ItemSelectedListener() {
            @Override
            public void onItemSelected(ListContainer listContainer, Component component, int index, long id) {
                if (index == selectedItemIndex) {
                    return;
                }
                if (isHandleSelect) {
                    itemSelectHandler.handleSelect(listContainer, selectedItemIndex, index);
                }
                if (selectStateChangedListener != null) {
                    selectStateChangedListener.onSelectStateChanged(CatListDialog.this, selectedItemIndex, index);
                }
                selectedItemIndex = index;
            }
        });
        return this;
    }

    public CatListDialog setItemClickedListener(ItemClickedListener listener) {
        itemClickedListener = listener;
        return this;
    }

    public CatListDialog setSelectStateChangedListener(OnSelectStateChangedListener selectStateChangedListener) {
        this.selectStateChangedListener = selectStateChangedListener;
        return this;
    }

    public CatListDialog setButton(int buttonNum, String text) {
        this.setButton(buttonNum, text, null);
        return this;
    }

    @Override
    public CatListDialog setButton(int buttonNum, String text, ClickedListener listener) {
        super.setButton(buttonNum, text, new ClickedListener() {
            @Override
            public void onClick(IDialog iDialog, int i) {
                if (listener != null) {
                    listener.onClick(iDialog, selectedItemIndex);
                }
                iDialog.hide();
            }
        });
        return this;
    }

    public CatListDialog handleSelect(boolean isHandleSelect) {
        this.isHandleSelect = isHandleSelect;
        return this;
    }

    @Override
    protected void onShowing() {
        super.onShowing();
        if (selectedItemIndex >= 0) {
            selectStateChangedListener.onSelectStateChanged(this, -1, selectedItemIndex);
        }
    }
}
