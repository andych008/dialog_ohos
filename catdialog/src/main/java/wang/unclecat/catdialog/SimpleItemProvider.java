package wang.unclecat.catdialog;

import ohos.agp.components.*;

import java.util.List;

/**
 * String类型的Provider
 *
 * @author: 喵叔catuncle
 */
public class SimpleItemProvider extends BaseItemProvider {

    public SimpleItemProvider(List<String> dataList) {
        this.dataList = dataList;
    }

    private List<String> dataList;

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int i) {
        return dataList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        ViewHolder viewHolder;

        if (component == null) {
            component = LayoutScatter.getInstance(componentContainer.getContext()).parse(ResourceTable.Layout_list_item, null, false);
            viewHolder = new ViewHolder();
            viewHolder.text = (Text) component.findComponentById(ResourceTable.Id_item_index);
            component.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)component.getTag();
        }

        viewHolder.text.setText(getItem(i).toString());

        return component;
    }

    private static final class ViewHolder {
        Text text;
    }


}
