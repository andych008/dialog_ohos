package wang.unclecat.catdialog;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

/**
 * 处理List item选中状态
 *
 * @author: 喵叔catuncle
 */
public class SimpleItemSelectHandler {
    private ShapeElement itemBg = new ShapeElement();

    public SimpleItemSelectHandler() {
        itemBg.setRgbColor(RgbColor.fromArgbInt(Color.LTGRAY.getValue()));
    }

    public void handleSelect(ListContainer listContainer, int preIndex, int index) {
        if (preIndex >= 0) {
            Component preComponent = listContainer.getComponentAt(preIndex);
            doSelect(preComponent, false);
        }

        if (index >= 0) {
            Component component = listContainer.getComponentAt(index);
            doSelect(component, true);
        }
    }

    protected void doSelect(Component component, boolean b) {
        if (component != null) {
            component.setSelected(b);
            if (b) {
                component.setBackground(itemBg);
            } else {
                component.setBackground(null);
            }
        }
    }
}
